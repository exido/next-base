/* eslint-disable no-ternary */
/* eslint-disable no-extra-parens */

import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'
import Icon from '@comps/icon'
import classNames from '@lib/classNames'

const profile = [
  {
    href: '/user/profile',
    icon: 'la-user',
    label: 'Profile'
  },
  {
    href: '/user/settings',
    icon: 'la-cog',
    label: 'Settings'
  },
  {
    href: '/signout',
    icon: 'la-sign-out-alt',
    label: 'Sign Out'
  }
]

export default function UserNav() {
  return (
    <Menu
      as="div"
      className="relative ml-3"
    >
      {({ open }) => (
        <>
          <div>
            <Menu.Button className="flex items-center max-w-xs text-sm bg-gray-800 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
              <span className="sr-only">Open user menu</span>

              <img
                alt="avatar"
                className="w-8 h-8 rounded-full"
                src="/images/default/avatar.jpg"
              />
            </Menu.Button>
          </div>

          <Transition
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
            show={open}
          >
            <Menu.Items
              className="absolute right-0 w-48 py-1 mt-2 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
              static
            >
              {profile.map((item) => (
                <Menu.Item key={item.label}>
                  {({ active }) => (
                    <a
                      className={classNames(
                        active
                          ? 'bg-gray-100'
                          : '',
                        'flex items-center space-x-2 px-4 py-1 text-sm text-gray-700'
                      )}
                      href={item.href}
                    >
                      <Icon
                        className="text-lg"
                        icon={item.icon}
                      />

                      <span>{item.label}</span>
                    </a>
                  )}
                </Menu.Item>
              ))}
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
  )
}