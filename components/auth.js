import { useEffect } from "react";
import { useRouter } from "next/router";
import { gql, useQuery } from "@apollo/client";

export default function Auth({ children }) {
  const router = useRouter();
  const { data, loading, error } = useQuery(
    gql`
      query ViewerQuery {
        viewer {
          id
        }
      }
    `
  );
  const viewer = data?.viewer;
  const shouldRedirect = !(loading || error || viewer);

  useEffect(() => {
    if (shouldRedirect) {
      router.push("/user/signin");
    }
  }, [shouldRedirect]);
  if (error) {
    return <p>{error.message}</p>;
  }
  if (viewer) {
    return children;
  }

  return <p>Loading...</p>;
}
