export default function Icon({ icon, className = "" }) {
  return <i className={`las ${icon} ${className}`} />;
}
