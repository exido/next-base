/* eslint-disable no-ternary */
import PropTypes from 'prop-types';
export default function Field({ name, label = '', type, autoComplete, required }) {
  return (
    <div>
      <label
        htmlFor={[
          name,
          'input'
        ].join('-')}
        id={[
          name,
          'label'
        ].join('-')}
      >
        {label} {required
          ? <span title="Required">*</span>
          : null}
      </label>
      <br />
      <input
        autoComplete={autoComplete}
        id={[
          name,
          'input'
        ].join('-')}
        name={name}
        required={required}
        type={type}
      />
    </div>
  );
}


Field.propTypes = {
  autoComplete: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  type: PropTypes.string.isRequired
};

Field.defaultProps = {
  autoComplete: false,
  label: null,
  required: false
};
