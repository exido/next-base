import PropTypes from 'prop-types';
export default function Container({ children, className }) {
  return <div className={`container mx-auto ${className}`}>{children}</div>;
}

Container.propTypes = {
  children: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired
};
