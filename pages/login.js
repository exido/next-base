import { gql, useApolloClient, useMutation } from '@apollo/client';
import Field from '@comps/field';
import Link from 'next/link';
import { getErrorMessage } from '@lib/form';
import { useRouter } from 'next/router';
import { useState } from 'react';

const SignInMutation = gql`
  mutation SignInMutation($email: String!, $password: String!) {
    signIn(input: { email: $email, password: $password }) {
      id
      email
    }
  }
`;

export default function SignIn() {
  const client = useApolloClient();
  const [signIn] = useMutation(SignInMutation);
  const [errorMsg, setErrorMsg] = useState();
  const router = useRouter();

  const handleSubmit = async (event) => {
    event.preventDefault();

    const emailElement = event.currentTarget.elements.email;
    const passwordElement = event.currentTarget.elements.password;

    try {
      await client.resetStore();
      const { data } = await signIn({
        variables: {
          email: emailElement.value,
          password: passwordElement.value
        }
      });

      if (data.signIn) {
        await router.push('/index');
      }
    } catch (error) {
      setErrorMsg(getErrorMessage(error));
    }
  };

  return (
    <>
      <h1>Sign In</h1>
      <form
        onSubmit={handleSubmit}
      >
        {errorMsg && <p>{errorMsg}</p>}
        <Field
          autoComplete="email"
          label="Email"
          name="email"
          required
          type="email"
        />
        <Field
          autoComplete="password"
          label="Password"
          name="password"
          required
          type="password"
        />
        <button type="submit">Sign in</button> or{' '}
        <Link href="/signup">
          <a>Sign up</a>
        </Link>
      </form>
    </>
  );
}

