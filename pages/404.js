export default function Error404() {
  return (
    <div
      className="flex items-center justify-center w-screen h-screen"
    >
      404
    </div>
  );
}
