/* eslint-disable class-methods-use-this */
import Document, { Head, Html, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  render() {
    return (
      <Html
        className="dark"
        lang="en"
      >
        <Head>
          <meta charSet="utf-8" />
          <meta
            content="ie=edge"
            httpEquiv="x-ua-compatible"
          />
          <meta
            content="index,follow,noodp"
            name="robots"
          />
          <meta
            content="index,follow"
            name="googlebot"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
