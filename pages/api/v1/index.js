import { ApolloServer } from 'apollo-server-micro';
import { applyMiddleware } from 'graphql-middleware';
import { getLoginSession } from '@lib/auth';
import permissions from '../../../apollo/permissions';
import schema from '../../../apollo/schema';


const apolloServer = new ApolloServer({
  async context(ctx) {
    const userSession = await getLoginSession(ctx.req);

    // Assign user to context if session is available
    if (userSession) {
      return {
        ...ctx,
        user: userSession
      };
    }

    return ctx;
  },
  // Apply Schema Permissions
  schema: applyMiddleware(schema, permissions)
});

export const config = {
  api: {
    bodyParser: false
  }
};

export default apolloServer.createHandler({ path: '/api/v1' });
