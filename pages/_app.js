/* eslint-disable react/require-default-props */
import '../styles/index.scss'
import { ApolloProvider } from '@apollo/client'
import Head from 'next/head'
import NProgress from 'nprogress'
import PropTypes from 'prop-types'
import Router from 'next/router'
import { useApollo } from '../apollo/client'

NProgress.configure({
  showSpinner: false
})
Router.events.on('routeChangeStart', () => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())
export default function App({ Component, pageProps }) {
  const apolloClient = useApollo(pageProps.initialApolloState)

  return (
    <ApolloProvider client={apolloClient}>
      <Head>
        <link
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
          rel="stylesheet"
        />
      </Head>

      <Component {...pageProps} />
    </ApolloProvider>
  )
}


App.propTypes = {
  Component: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  pageProps: PropTypes.object
}
