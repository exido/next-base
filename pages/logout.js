import { gql, useApolloClient, useMutation } from '@apollo/client';
import { useEffect } from 'react';
import { useRouter } from 'next/router';


const SignOutMutation = gql`
  mutation SignOutMutation {
    signOut
  }
`;

export default function SignOut() {
  const client = useApolloClient();
  const router = useRouter();
  const [signOut] = useMutation(SignOutMutation);

  useEffect(() => {
    signOut().then(() => {
      client.resetStore().then(() => {
        router.push('/');
      });
    });
  }, [signOut, router, client]);

  return <p>Signing out...</p>;
}

