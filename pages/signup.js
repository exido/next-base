import { gql, useMutation } from '@apollo/client';
import Field from '@comps/field';
import Link from 'next/link';
import { getErrorMessage } from '@lib/form';
import { useRouter } from 'next/router';
import { useState } from 'react';

const SignUpMutation = gql`
  mutation SignUpMutation($email: String!, $password: String!) {
    signUp(input: { email: $email, password: $password }) {
      user {
        id
        email
      }
    }
  }
`;

export default function SignUp() {
  const [signUp] = useMutation(SignUpMutation);
  const [errorMsg, setErrorMsg] = useState();
  const router = useRouter();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const emailElement = event.currentTarget.elements.email;
    const passwordElement = event.currentTarget.elements.password;

    try {
      await signUp({
        variables: {
          email: emailElement.value,
          password: passwordElement.value
        }
      });

      router.push('/signin');
    } catch (error) {
      setErrorMsg(getErrorMessage(error));
    }
  };

  return (
    <>
      <h1>Sign Up</h1>
      <form onSubmit={handleSubmit}>
        {errorMsg && <p>{errorMsg}</p>}
        <Field
          autoComplete="email"
          label="Email"
          name="email"
          required
          type="email"
        />
        <Field
          autoComplete="password"
          label="Password"
          name="password"
          required
          type="password"
        />
        <button type="submit">Sign up</button> or{' '}
        <Link href="signin">
          <a>Sign in</a>
        </Link>
      </form>
    </>
  );
}

