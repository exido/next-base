module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#AF3963",
        secondary: "#FBD2D0",
        navy: "#242635",
        black: "#222",
      },
      fontFamily: {
        sans: ['"ExivarSans"', "sans-serif"],
        serif: ['"ExivarSerif"', "sans-serif"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
