import { gql } from "@apollo/client";

module.exports = gql`
  type User {
    id: ID!
    email: String!
    createdAt: String
    updatedAt: String
    active: Boolean
  }

  input SignInUpInput {
    email: String!
    password: String!
  }

  type Query {
    user(id: ID!): User!
    users: [User]!
    viewer: User
  }

  type Mutation {
    signUp(input: SignInUpInput!): User!
    signIn(input: SignInUpInput!): User!
    signOut: Boolean!
  }
`;
