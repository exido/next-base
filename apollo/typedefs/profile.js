import { gql } from "@apollo/client";

module.exports = gql`
  type Profile {
    userId: ID!
    fullName: String
    avatar: String
  }
`;
