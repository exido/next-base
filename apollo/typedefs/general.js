import { gql } from "@apollo/client";

module.exports = gql`
  type Query {
    customErrorInResolver: String
    customErrorInRule: String
  }
`;
