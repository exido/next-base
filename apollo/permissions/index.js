import { rule, shield, allow, deny } from 'graphql-shield';

const isAuthenticated = rule()((_p, __a, { user }) => user !== null);
const can = (permission) => rule()((_p, __a, { user }) => isAuthenticated && user?.can(permission));

const permissions = shield({
  Mutation: {
    '*': allow,
    signIn: allow,
    signOut: allow,
    signUp: allow
  },
  Query: {
    '*': allow,
    viewer: isAuthenticated
  }
});

export default permissions;
