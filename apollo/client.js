import { ApolloClient, InMemoryCache } from '@apollo/client';
import merge from 'deepmerge';
import { useMemo } from 'react';

let apolloClient = null;

const createIsomorphLink = () => {
  if (typeof window === 'undefined') {
    const { SchemaLink } = require('@apollo/client/link/schema');
    const { schema } = require('./schema');

    return new SchemaLink({ schema });
  }

  const { HttpLink } = require('@apollo/client/link/http');

  return new HttpLink({
    credentials: 'same-origin',
    uri: '/api/v1'
  });

};

const createApolloClient = () => new ApolloClient({
  cache: new InMemoryCache(),
  link: createIsomorphLink(),
  ssrMode: typeof window === 'undefined'
});


export const initializeApollo = (initialState = null) => {
  // eslint-disable-next-line no-underscore-dangle
  const _apolloClient = apolloClient ?? createApolloClient();

  /**
   * If your page has Next.js data fetching methods that use Apollo Client, the initial state
   * Get hydrated here
   */
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache);

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') {
    return _apolloClient;
  }
  // Create the Apollo Client once in the client
  if (!apolloClient) {
    apolloClient = _apolloClient;
  }

  return _apolloClient;
};

export const useApollo = (initialState) => useMemo(() => initializeApollo(initialState), [initialState]);
