import {
  makeExecutableSchema,
  mergeResolvers,
  mergeTypeDefs
} from 'graphql-tools';

export default makeExecutableSchema({
  resolvers: mergeResolvers([require('./resolvers/user')]),
  typeDefs: mergeTypeDefs([require('./typedefs/user')])
});
