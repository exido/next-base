import {
  AuthenticationError,
  UserInputError,
  ForbiddenError,
} from "apollo-server-micro";
import {
  createUser,
  getRolePermissions,
  validatePassword,
} from "../../lib/user";
import { setLoginSession } from "@lib/auth";
import { removeTokenCookie } from "../../lib/auth-cookies";
import prisma from "@db";

module.exports = {
  Query: {
    async viewer(_parent, _args, { user }, _info) {
      if (!user) return null;
      try {
        return prisma.user.findUnique({
          where: {
            id: user.id,
          },
        });
      } catch (error) {
        console.log(error);
      }
    },
  },
  Mutation: {
    async signUp(_parent, { input }, _context, _info) {
      try {
        return createUser(input);
      } catch (error) {
        throw new UserInputError(error.message);
      }
    },
    async signIn(_parent, { input }, context, _info) {
      const user = await prisma.user.findUnique({
        where: {
          email: input?.email || undefined,
        },
        include: {
          UserRole: {
            include: {
              Role: {
                include: {
                  RolePermission: {
                    include: {
                      Permission: {
                        select: {
                          name: true,
                          read: true,
                          create: true,
                          update: true,
                          delete: true,
                          relation: true,
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      });
      if (user && (await validatePassword(user, input.password))) {
        const session = {
          id: user.id,
          email: user.email,
          ...getRolePermissions(user),
        };
        await setLoginSession(context.res, session);
        return user;
      }

      throw new UserInputError("Invalid email and password combination");
    },
    async signOut(_parent, _args, context, _info) {
      removeTokenCookie(context.res);
      return true;
    },
  },
};
