/* eslint-disable consistent-return */
import { MAX_AGE, getTokenCookie, setTokenCookie } from './auth-cookies';
import Iron from '@hapi/iron';

const { TOKEN_SECRET } = process.env;
const toSecond = 1000;

export const setLoginSession = async (res, session) => {
  const createdAt = Date.now();
  // Create a session object with a max age that we can validate later
  const obj = {
    ...session,
    createdAt,
    maxAge: MAX_AGE
  };
  const token = await Iron.seal(obj, TOKEN_SECRET, Iron.defaults);

  setTokenCookie(res, token);
};

export const getLoginSession = async (req) => {
  const token = getTokenCookie(req);

  if (!token) {
    return;
  }

  try {
    const session = await Iron.unseal(token, TOKEN_SECRET, Iron.defaults);

    // Validate the expiration date of the session
    if (Date.now() < session.createdAt + session.maxAge * toSecond) {
      const user = {
        ...session,
        can: (permission) => user.permissions.includes(permission),
        is: (role) => user.roles.includes(role)
      };

      return user;
    }
  } catch (error) {
    return null;
  }
};

