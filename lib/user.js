import crypto from "crypto";
import prisma from "@db";
import moment from "moment";

export async function createUser({ email, password }) {
  const salt = crypto.randomBytes(16).toString("hex");
  const hash = crypto
    .pbkdf2Sync(password, salt, 1000, 64, "sha512")
    .toString("hex");
  const userObj = {
    email,
    hash,
    salt,
    createdAt: moment().toISOString(),
    active: false,
  };
  const userExists = await prisma.user.findUnique({
    where: {
      email,
    },
  });
  if (userExists) {
    throw new Error("User Exists");
  }
  try {
    return prisma.user.create({
      data: {
        ...userObj,
        UserRole: {
          create: {
            roleName: "user",
          },
        },
        Profile: {
          create: {
            fullName: null,
            avatar: null,
          },
        },
      },
    });
  } catch (error) {
    throw new Error("Server Error, Plesae contact admin");
  }
}

// Compare the password of an already fetched user (using `findUser`) and compare the
// password for a potential match
export async function validatePassword(user, inputPassword) {
  const inputHash = crypto
    .pbkdf2Sync(inputPassword, user.salt, 1000, 64, "sha512")
    .toString("hex");
  const passwordsMatch = user.hash === inputHash;
  return passwordsMatch;
}

export function getRolePermissions(user) {
  const role = [user.UserRole.roleName];
  const permissions = [];
  user.UserRole.Role.RolePermission.map((item) => {
    item.Permission.create &&
      permissions.push(
        `create:${item.Permission.relation}:${item.Permission.name}`
      );
    item.Permission.read &&
      permissions.push(
        `read:${item.Permission.relation}:${item.Permission.name}`
      );
    item.Permission.update &&
      permissions.push(
        `update:${item.Permission.relation}:${item.Permission.name}`
      );
    item.Permission.delete &&
      permissions.push(
        `delete:${item.Permission.relation}:${item.Permission.name}`
      );
  });
  return {
    role,
    permissions,
  };
}
